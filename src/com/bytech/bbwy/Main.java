package com.bytech.bbwy;

/*The MIT License (MIT)

Copyright (c) 2014 Jakewp11

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/


import java.util.Locale;

import com.bytech.bbwy.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.view.View;
/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */


public class Main extends Activity {
    WebView webView;


	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);
        webView = (WebView)findViewById(R.id.fullscreen_content);
        webView.setWebViewClient(new MyWebViewClient());
        if(isZh())
        	webView.loadUrl("file:///android_asset/www/index.html");
        else
        	webView.loadUrl("file:///android_asset/www-en/index.html");
        //you can also link to a website. Example:
        //webView.loadUrl("www.google.com");
        //I have included web permissions in the AndroidManifest.xml
        webView.setWebChromeClient(new WebChromeClient());
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setDatabasePath("/data/data/"+this.getPackageName()+"/databases/");

        webView.addJavascriptInterface(new JsOperation(this), "client");
        
        initAdmob();
    }
	
	private boolean isZh() {
        Locale locale = getResources().getConfiguration().locale;
        String language = locale.getLanguage();
        if (language.endsWith("zh"))
            return true;
        else
            return false;
    }
    
    @Override
    public void onBackPressed()
    {
        if(webView.canGoBack())
            webView.goBack();
        else
            super.onBackPressed();
    }
    
    public LinearLayout adbar;
  //Bely
    private void initAdmob(){
    	
    	
    	// 创建adView。
    	AdView adView = new AdView(this);
    	adbar = (LinearLayout)findViewById(R.id.adbar);
    	//adView = (AdView)findViewById(R.id.admobview);
        adView.setAdUnitId("ca-app-pub-0951359270204907/6357891850");
        adView.setAdSize(AdSize.SMART_BANNER);

        // 查询LinearLayout，假设其已指定
        // 属性android:id="@+id/mainLayout"。
        //RelativeLayout layout = (RelativeLayout)findViewById(R.id.fullscreen_relativelayout);

        //RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 60);//RelativeLayout.LayoutParams.WRAP_CONTENT);
        //lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        //lp.addRule(RelativeLayout.ABOVE, R.id.BottomBarLayout);
        //adView.setVisibility(View.GONE);
        //adView.setLayoutParams(lp);
        // 在其中添加adView。
        
        adbar.addView(adView);
        
        /*ImageView iv = new ImageView(this);
        iv.setImageResource(R.drawable.righttool);
        mLayout_AdmobBar.addView(iv);*/

        // 启动一般性请求。
        AdRequest adRequest = new AdRequest.Builder().build();
        
        /*AdRequest adRequest = new AdRequest.Builder()
        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)       // 模拟器
        .addTestDevice("15DE390AA17A7EE03C8C522B9D6F1F29") // 我的Galaxy Nexus测试手机
        .build();*/
        
       // AdRequest.Builder.addTestDevice("15DE390AA17A7EE03C8C522B9D6F1F29")

        // 在adView中加载广告请求。
        adView.loadAd(adRequest);

       // adbar.setVisibility(View.INVISIBLE);
    }
    

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    
    class JsOperation {
    	 
    	Main mActivity;
 
        public JsOperation(Main activity) {
        	
            mActivity = activity;
        }
 
//    测试方法
   
        public void close() {
        	mActivity.finish();
        	
        }
        
        public void showAD(){
        	mActivity.adbar.setVisibility(View.VISIBLE);
        }
    }
}
